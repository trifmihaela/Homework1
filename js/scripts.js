var myButton = document.querySelector(".myBtn");
var textarea = document.querySelector(".myTextarea");

function checksum(){
//number of original words

  var numberOriginalWords = textarea.value.replace(/[ ]{2,}/gi," ").trim().split(' ').length;

  //words only with alphabetical characters and 10 char long

  var newWords = textarea.value.replace(/[^a-z0-9+]+/gi, '').match(/.{1,10}/g);

  //capitalize each word

for (var i=0;i<newWords.length;i++){
  var firstLetter = newWords[i].charAt(0);
  newWords[i] = newWords[i].replace(firstLetter,firstLetter.toUpperCase());
}

// Upcase any vowel that is after two consonants and previous vowel is upcase.

    var uppercaseVowels = 0;
    for (var i=0;i<newWords.length;i++){
        if (newWords[i].match(/[AEIOU]/)) { uppercaseVowels ++;
           for (var j=i+2 ; j <newWords[i].length ; j++){
               if (newWords[i].charAt(j).match(/[aeiou]/)) {
                   if ( !newWords[i].charAt(j-1).match(/[aeiouAEIOU]/) && !newWords[i].charAt(j-2).match(/[aeiouAEIOU]/)){
                	    newWords[i] = newWords[i].replace(newWords[i].charAt(j),newWords[i].charAt(j).toUpperCase());
                      uppercaseVowels ++;
                   }
                }
       }
    }
  }

// number of consonants

  var consonants = 0;
  var string = textarea.value;
  for (var i=0;i<string.length;i++){
        if(string[i].charAt().match(/[bcdfghjklmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ]/)){
          consonants++;
        }
     }

//Length of original string

var OriginalString = textarea.value.length;

var finalResult = numberOriginalWords + " - "+ newWords.length + " - " +uppercaseVowels + " - " + consonants + " - "+ OriginalString;
console.log(finalResult);


}
